def perimRectangle(w, h):
    """Calculate the perimeter of a rectangle
    :param w: width
    "param h: height
    :return: perimeter
    """
    return 2*w + 2*h

def perimSquare(size):
    perimRectangle(size, size)

print(perimSquare(50))