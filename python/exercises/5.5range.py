for i in [0, 1, 2, 3, 4]:   # repeat 5 times
    print('*', end='')      # end of an empty string will keep cursor on the same line
print()                     # put cursor on the next line