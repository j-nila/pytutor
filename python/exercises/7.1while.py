def sumTo(bound):
  """ Return the sum of 1+2+3 ... bound """
  sum  = 0
  num = 1
  while num <= bound:
    sum = sum + num
    num = num + 1
  return sum

print(sumTo(4))
print(sumTo(50))