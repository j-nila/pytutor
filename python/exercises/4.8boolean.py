def isDivisible(x, y):
    return x % y == 0

if isDivisible(10, 5):
    print("That works")
else:
    print("Those values are no good")