def square(x):
    y = x * x
    return y

def sumOfSquares(x, y, z):
    a = square(x)
    b = square(y)
    c = square(z)

    return a + b + c

a = -5
b = 2
c = 10
result = sumOfSquares(a, b, c)
print(result)