day = "Thursday"
print(day)
day = "Friday"
print(day)
# the next line is not good coding style!
# A variable should not change its meaning part way through a program
day = 21
print(day)