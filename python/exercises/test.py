#to run doctest: python -m doctest -v test.py

import math

def volume_sphere(r):
    '''
    Calculates the volume of a sphere with radius r
    :param r: radius
    :return: volume of sphere

    >>> volume_sphere(6.65)
    1231.84

    '''

    return round((4/3)*math.pi*(r**3), 2)

def circum_sphere(r):
    '''
    Calculates the circumference of a sphere with radius r
    :param r: radius
    :return: circumference of sphere

    >>> circum_sphere(6.65)
    41.78

    '''

    return round(2*math.pi*r, 2)

if __name__ == '__main__':
  radius = float(input('Enter your radius: '))
  print(f'The volume is {volume_sphere(radius)}')