def square(x):
    y = x * x
    return y

toSquare = 10
result = square(toSquare)
print(f'The result of {toSquare} squared is {result}')