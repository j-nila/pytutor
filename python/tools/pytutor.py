# By Tyler Caraza-Harter
# https://github.com/tylerharter/caraza-harter-com/
# Change by Jaya: to generate complete HTML document, not just the div

import os, sys, json, subprocess
from subprocess import check_output

PYTUTOR = "python/tools/generate_json_trace.py"

EMBEDDING = """
<!DOCTYPE html>
<html>

<!--
  Python Tutor: https://github.com/pgbovine/OnlinePythonTutor/
  Copyright (C) Philip Guo (philip@pgbovine.net)
  LICENSE: https://github.com/pgbovine/OnlinePythonTutor/blob/master/LICENSE.txt
-->

<head>
  <title>DIV</title>
  
  <script type="text/javascript" src="build/pytutor-embed.bundle.js" charset="utf-8"></script>

</head>

<body style="background-color: white;">
<div id="DIV"></div>
<script type="text/javascript">
  var trace = TRACE;
  addVisualizerToPage(trace, 'DIV',  {startingInstruction: 0, hideCode: false, lang: "py3", disableHeapNesting: true});
</script>
<footer style="color: #999; font-size: 10pt;">
  <p style="margin-top: 30px;">This tool uses source code
    modified/extended from Tyler Caraza-Hunter (https://github.com/tylerharter/caraza-harter-com) who based their work on Online Python Tutor (Philip Guo, GPL 3,
    https://github.com/pgbovine/OnlinePythonTutor/) and Pyodide (MPL-2.0,
    https://github.com/pyodide/pyodide).  The authors of those projects
    are not affiliated with or responsible for this modified version.</p>
</footer>
</body>
</html>
"""

def run_pytutor(py):
    try:
        js = check_output(["python", PYTUTOR, py])
    except subprocess.CalledProcessError as e:
        js = e.output
    return json.dumps(json.loads(js))

def main():
    if len(sys.argv) < 2:
        print("Usage: python pytutor.py file1.py [file2.py, ...]")
        sys.exit(1)

    for py in sys.argv[1:]:
        js = run_pytutor(py)
        div = py.replace(".", "_").replace("/", "_")
        code = EMBEDDING.replace("DIV", div).replace("TRACE", js)
        print(code)


if __name__ == '__main__':
     main()